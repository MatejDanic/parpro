#
#       main.py
#
#       Pokretanjem iz datoteke main2.py se dobije nesto detaljniji ispis sa trenutnim stanjima vilica
#

from mpi4py import MPI
import random
import time
import sys


class Philosopher:
    def __init__(self, index):
        # Philosopher rank
        self.index = index
        # Indication of fork possession (0 - no fork, 1 - dirty fork, 2 - clean fork)
        self.left_fork = 0
        self.right_fork = 0
        # Ranks of philosopher's neighbours, first initialized at -1, then set for correct rank before the start
        self.left_neigh = -1
        self.right_neigh = -1
        # Request memorization (0 - no request, 1 - pending request for fork)
        self.left_request = 0
        self.right_request = 0

    def __repr__(self):
        return "\nneigbour:" + str(self.left_neigh) + " | fork:" + str(
            self.left_fork) + " <- [rank:" + str(self.index) + "] -> fork:" \
               + str(self.right_fork) + " | neighbour:" + str(self.right_neigh)

    def output(self, message):
        print(self.index * "\t" + message)
        sys.stdout.flush()


# MPI initialization
comm = MPI.COMM_WORLD
phil = Philosopher(comm.Get_rank())
size = comm.size

# Assuming there are at least two philosophers
if size < 2:
    print("Cannot have less than two philosophers!")
    exit(1)

# Configuring neighbourhoods
if phil.index == 0:
    phil.left_neigh = size - 1
    phil.right_neigh = phil.index + 1
elif phil.index == size - 1:
    phil.right_neigh = 0
    phil.left_neigh = phil.index - 1
else:
    phil.right_neigh = phil.index + 1
    phil.left_neigh = phil.index - 1

# Shared forks are in possession of the philosopher with the lower index
# First philosopher has two forks (left and right), last has zero and all others have one (right)
if phil.index == 0:
    phil.left_fork = 1
    phil.right_fork = 1
elif phil.index < comm.size - 1:
    phil.right_fork = 1


def think(num_of_seconds):
    #phil.output("I'm thinking for " + str(num_of_seconds) + " seconds...")
    phil.output("mislim")
    while num_of_seconds > 0:  # checking and responding to requests (tag=1 - fork request, tag=4 - sending fo(u)rk)

        if comm.iprobe(source=phil.left_neigh, tag=1):  # Check if there is a requests from left neighbour
            received_data = comm.recv(source=phil.left_neigh, tag=1)
            if received_data == 2:  # Data check only important for when there are only two philosophers,
                # because left and right neighbours would be the same
                phil.left_request = 1
            else:
                phil.right_request = 1
        if comm.iprobe(source=phil.right_neigh, tag=1):  # Check if there is a requests from right neighbour
            received_data = comm.recv(source=phil.right_neigh, tag=1)
            if received_data == 1:  # Data check only important for when there are only two philosophers,
                # because left and right neighbours would be the same
                phil.right_request = 1
            else:
                phil.left_request = 1

        if phil.left_request:
            #phil.output("Others are looking for my left fork.")
            if phil.left_fork != 0:  # If I have a left fork I will send it because I'm currently thinking
                #phil.output("I'm sending my left fork!")
                phil.left_fork = 2  # Cleaning
                comm.send(phil.left_fork, dest=phil.left_neigh, tag=4)
                phil.left_request = 0  # I responded to the request
                phil.left_fork = 0  # I don't have the fork anymore
        if phil.right_request:
            #phil.output("Others are looking for my right fork.")
            if phil.right_fork != 0:  # If I have a right fork I will send it because I'm currently thinking
                #phil.output("I'm sending my right fork!")
                phil.right_fork = 2  # Cleaning
                comm.send(phil.right_fork, dest=phil.right_neigh, tag=4)
                phil.right_request = 0  # I responded to the request
                phil.right_fork = 0  # I don't have the fork anymore
        time.sleep(1)  # Wait for one second, then check again
        num_of_seconds -= 1


def get_forks():
    while phil.left_fork == 0 or phil.right_fork == 0:  # Repeating process while both forks are not in possession
        left = False  # Currently looking for left fork
        right = False  # Currently looking for right fork
        if phil.left_fork == 0:
            left = True
            data = 1  # Data to send to left neighbour in request for left fork,
            # only important for case with two philosophers
            src = phil.left_neigh
            #phil.output("I'm looking for my left fork!")
        elif phil.right_fork == 0:
            right = True
            data = 2  # Data to send to left neighbour in request for left fork,
            # only important for case with two philosophers
            src = phil.right_neigh
            #phil.output("I'm looking for my right fork!")
        comm.send(data, dest=src, tag=1)
        phil.output("trazim vilicu (" + str(phil.index) + ")")
        while left or right:  # If both become false, that means I have found what I was looking for
            st = MPI.Status()  # status for checking tags and source of requests
            #if left:
                #phil.output("I'm waiting for a response for my left fork...")
            #elif right:
                #phil.output("I'm waiting for a response for my right fork...")

            data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=st)  # Wait for any request
            if st.Get_tag() == 4:  # If tag == 4 that means someone is sending a fork
                if st.Get_source() == phil.left_neigh and phil.left_fork == 0 and left:
                    # If source is my left neighbour, I don't have a left fork and I'm looking for it
                    phil.left_fork = data
                    #phil.output("I have received my left fork.")
                    left = False
                if st.Get_source() == phil.right_neigh and phil.right_fork == 0 and right:
                    # If source is my right neighbour, I don't have a right fork and I'm looking for it
                    phil.right_fork = data
                    #phil.output("I have received my right fork.")
                    right = False
            elif st.Get_tag() == 1:  # If tag == 1, someone is requesting a fork
                if st.Get_source() == phil.left_neigh and data == 2 or phil.left_request == 1:
                    #  If source is my left neighbour or he already requested my left fork
                    #phil.output("Others are looking for my left fork.")
                    if phil.left_fork == 1:  # If I have the fork and it is dirty
                        #phil.output("I'm sending my left fork!")
                        phil.left_fork = 2
                        comm.send(phil.left_fork, dest=phil.left_neigh, tag=4)
                        phil.left_fork = 0
                    elif phil.left_fork == 2:  # If I have the fork and it is clean
                        #phil.output("My left fork is clean, I won't send it yet.")
                        phil.left_request = 1
                    else:
                        phil.left_request = 1
                elif st.Get_source() == phil.right_neigh and data == 1 or phil.right_request == 1:
                    #  If source is my right neighbour or he already requested my right fork
                    #phil.output("Others are looking for my right fork.")
                    if phil.right_fork == 1:  # If I have the fork and it is dirty
                        #phil.output("I'm sending my right fork.")
                        phil.right_fork = 2
                        comm.send(phil.right_fork, dest=phil.right_neigh, tag=4)
                        phil.right_fork = 0
                    elif phil.right_fork == 2:  # If I have the fork and it is clean
                        #phil.output("My right fork is clean, I won't send it yet.")
                        phil.right_request = 1
                    else:
                        phil.right_request = 1


def eat(num_of_seconds):
    #phil.output("I'm eating for " + str(num_of_seconds) + " seconds...")
    phil.output("jedem")
    time.sleep(num_of_seconds)
    # Forks become dirty after eating
    phil.left_fork = 1
    phil.right_fork = 1
    #phil.output("I have finished eating.")

    # Answer pending requests if there are any
    if comm.iprobe(source=phil.left_neigh, tag=1):  # Check if there are pending requests from left neighbour
        received_data = comm.recv(source=phil.left_neigh, tag=1)
        if received_data == 2:
            phil.left_request = 1
        else:
            phil.right_request = 1
    if comm.iprobe(source=phil.right_neigh, tag=1):  # Check if there are pending requests from right neighbour
        received_data = comm.recv(source=phil.right_neigh, tag=1)
        if received_data == 1:
            phil.right_request = 1
        else:
            phil.left_request = 1

    if phil.left_request:
        #phil.output("There is a pending request for my left fork, I'm sending it!")
        phil.left_fork = 2  # Clean before sending
        comm.send(phil.left_fork, dest=phil.left_neigh, tag=4)
        phil.left_fork = 0  # Remove from possession
        phil.left_request = 0  # Responded to request
    if phil.right_request:
        #phil.output("There is a pending request for my right fork, I'm sending it!")
        phil.right_fork = 2  # Clean before sending
        comm.send(phil.right_fork, dest=phil.right_neigh, tag=4)
        phil.right_fork = 0  # Remove from possession
        phil.right_request = 0  # Responded to request


while True:  # Repeat until user interrupts
    think(random.randint(5, 10))
    get_forks()
    eat(random.randint(5, 10))
