#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

struct Phil
{
	// Rank of philosopher
	int index;
	// Ranks of philosopher neighbors
	int left_neighbor;
	int right_neighbor;
	// Indication of fork possesion (0 - no fork, 1 - dirty fork, 2 - clean fork)
	int left_fork;
	int right_fork;
	// Indication of request type (1 - right neighbor needs his left fork, 2 - left neighbor needs his right fork, 3 - both)
	int request;
};

struct Phil think(struct Phil phil, int num_of_seconds) {
	int flag = 0;
	int recieve_data = 0;
	printf("\n%d I'm thinking for %d seconds...", phil.index, num_of_seconds);
	while (num_of_seconds > 0) {
		// Check if there are any pending requests
		MPI_Status status;
		// Tags indicate message type (1 - fork request, 2, sending fork)
		MPI_Iprobe(MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &flag, &status);
		printf("\n%d Flag = %d", phil.index, flag);
		if (flag) {
			printf("\n%d I received a request", phil.index);
			MPI_Recv(&recieve_data, 1, MPI_INT, status.MPI_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			if (recieve_data == 1) {
				// Do I have right fork and is it dirty (1)
				if (phil.right_fork == 1) {
					printf("\n%d Sending dirty fork to %d", phil.index, phil.right_neighbor);
					int send = 4;
					MPI_Send(&send, 1, MPI_INT, phil.right_neighbor, 2,
						MPI_COMM_WORLD);
					phil.right_fork = 0;
				}
				else if (phil.request != 2) {
					phil.request = recieve_data;
				}
				else {
					phil.request = 3;
				}
			}
			else if (recieve_data == 2) {
				// Do I have left fork and is it dirty (1)
				if (phil.left_fork == 1) {
					printf("\n%d Sending dirty fork to %d", phil.index, phil.left_neighbor);
					int send = 4;
					MPI_Send(&send, 1, MPI_INT, phil.left_neighbor, 2,
						MPI_COMM_WORLD);
					phil.left_fork = 0;
				}
				else if (phil.request != 1) {
					phil.request = recieve_data;
				}
				else {
					phil.request = 3;
				}
			}
		}
		// Think for 1 second then check again
		Sleep(1000);
		num_of_seconds--;
	}
	return phil;
}

struct Phil get_forks(struct Phil phil) {
	int send = 0;
	int data = 0;
	MPI_Request request;
	// Sending requests for missing forks
	if (!phil.left_fork) {
		printf("\n%d I'm looking for my left fork.", phil.index);
		send = 1;
		MPI_Isend(&send, 1, MPI_INT, phil.left_neighbor, 1,
			MPI_COMM_WORLD, &request);
	}
	if (!phil.right_fork) {
		printf("\n%d I'm looking for my right fork.", phil.index);
		send = 2;
		MPI_Isend(&send, 1, MPI_INT, phil.right_neighbor, 1,
			MPI_COMM_WORLD, &request);
	}
	int receive_data = 0;
	while (!phil.left_fork | !phil.right_fork) {
		MPI_Status status;
		int flag = 0;
		if (checkMessages(phil)) {

			MPI_Recv(&receive_data, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

			// Four indicates sending clean fo(u)rk
			if (receive_data == 4) {
				// Check which neigbor sent it
				if (status.MPI_SOURCE == phil.left_neighbor) {
					printf("\n%d I received my left fork.", phil.index);
					phil.left_fork = 2;
				}
				else {
					printf("\n%d I received my right fork.", phil.index);
					phil.right_fork = 2;
				}
			}
			else if (phil.request == 1 || phil.request == 2) {
				printf("\n%d Others are looking for my fork.", phil.index);
				phil.request = 3;
			}
			else {
				printf("\n%d Others are looking for my fork.", phil.index);
				phil.request = receive_data;
			}
		}
	}
	return phil;
}

int checkMessages(struct Phil phil) {
	int flag = 0;
	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
	//printf("\n%d Checking messages", phil.index);
	Sleep(10);
	return flag;
}

struct Phil eat(struct Phil phil, int num_of_seconds) {
	printf("\n%d I'm eating for %d seconds...", phil.index, num_of_seconds);
	// Eat
	Sleep(num_of_seconds * 1);
	// Forks are dirty from eating
	phil.left_fork = 1;
	phil.right_fork = 1;
	// Send clean fo(u)rks if there are requests for them
	if (phil.request == 1) {
		phil.left_fork = 0;
		int send = 4;
		MPI_Send(&send, 1, MPI_INT, phil.left_neighbor, 2,
			MPI_COMM_WORLD);
	}
	else if (phil.request == 2) {
		phil.right_fork = 0;
		int send = 4;
		MPI_Send(&send, 1, MPI_INT, phil.right_neighbor, 2,
			MPI_COMM_WORLD);
	}
	else if (phil.request == 3) {
		phil.left_fork = 0;
		phil.right_fork = 0;
		int send = 2;
		MPI_Send(&send, 1, MPI_INT, phil.left_neighbor, 0,
			MPI_COMM_WORLD);
		MPI_Send(&send, 1, MPI_INT, phil.right_neighbor, 0,
			MPI_COMM_WORLD);
	}
	return phil;
}

int main2(int argc, char** argv) {

	// Initialize the MPI environment
	MPI_Init(NULL, NULL);
	// Find out rank, size
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// We are assuming at least 3 philosophers for this task
	if (world_size < 2) {
		fprintf(stderr, "World size must be two or larger for %s\n", argv[0]);
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	// Setting right and left neighbors of all philosophers
	struct Phil phil = { .index = world_rank };
	phil.left_neighbor = phil.index - 1;
	phil.right_neighbor = phil.index + 1;
	if (phil.index == 0) {
		phil.left_neighbor = world_size - 1;
	}
	else if (phil.index == world_size - 1) {
		phil.right_neighbor = 0;
	}


	phil.left_fork = 0;
	phil.right_fork = 0;
	// First philosopher has left and right forks
	if (phil.index == 0) {
		phil.left_fork = 1;
		phil.right_fork = 1;
	} // Every other philosopher except last has right fork
	else if (phil.index < world_size - 1) {
		phil.right_fork = 1;
	}

	printf("\n%d <- [%d] -> %d : [%d %d]", phil.left_neighbor, phil.index, phil.right_neighbor, phil.left_fork, phil.right_fork);


	srand(time(NULL)*world_rank);
	int r = rand();
	int n = 2;
	// Stop when you've eaten n times
	while (n > 0) {

		phil = think(phil, rand() % 3 + 1);

		/*if (phil.index == 2) {
			printf("\n%d Sending...", phil.index);
			int send = 1;
			MPI_Send(&send, 1, MPI_INT, phil.left_neighbor, 0,
				MPI_COMM_WORLD);
			send = 2;
			MPI_Send(&send, 1, MPI_INT, phil.right_neighbor, 0,
				MPI_COMM_WORLD);
			int data;
			MPI_Status status;
			MPI_Recv(&data, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			printf("%d I received %d", phil.index, data);
			if (status.MPI_SOURCE == phil.left_neighbor) {
				phil.left_fork = 2;
			}
			else {
				phil.right_fork = 2;
			}

		}*/
		phil = get_forks(phil);
		//phil = eat(phil, rand() % 3 + 1);
		n--;
		printf("\n%d <- [%d] -> %d : [%d %d]", phil.left_neighbor, phil.index, phil.right_neighbor, phil.left_fork, phil.right_fork);
	}

	MPI_Finalize();
	return 0;
}