from mpi4py import MPI
import time, sys

BOARD_WIDTH = 7


def check_winner(test_board, last_col):  # method for determining if last entered column won the game
    last_row = len(test_board[last_col]) - 1  # index of last entered row is one less than length of column
    check_lists = [test_board[last_col]]
    # check_lists will be a collection of all possible combinations which could have a winning line
    # this includes the column itself
    row_list, diag_1, diag_2 = {}, {}, {}  # also the whole row and two diagonals
    var_x, var_y = last_row - last_col, last_row + last_col
    # variables for determining if position is on the same diagonal
    # diff of row and col indices (var_x) is for upwards diagonal
    # sum of row and col indices (var_y) is for downwards diagonal
    for n, col in enumerate(test_board):  # loop over board columns and their indices
        if last_row in col:  # if column has item in position of last entered row
            row_list[n] = col[last_row]  # add to row_list dict
    # using dict solves the problem of rows with missing items, ex. "= C = C C C P"
    # every item is stored in dict with their column index as the key
    # this is important in the win_cond function below
    for col in range(BOARD_WIDTH):  # for every column in board
        for row in range(last_row - 7, last_row + 7):  # rows in diagonal can be anywhere from 7 below to 7 above
            if row in test_board[col]:  # if column has item in row
                if row - col == var_x:  # if item is on the same upwards diagonal
                    diag_1[col] = test_board[col][row]  # add item to diagonal
                if row + col == var_y:  # if item is on the same downwards diagonal
                    diag_2[col] = test_board[col][row]  # add to diagonal
            # diagonals are also dict where key of item is column index
    # add row and two diagonals in check_lists
    check_lists.append(diag_1)
    check_lists.append(diag_2)
    check_lists.append(row_list)
    for cl in check_lists:  # for every possible winning line
        if len(cl) >= 4:  # if it is 4 or longer (need 4 to connect4)
            result = win_cond(cl)  # test it
            if result != "0":  # if result is other than '0' ('P' or 'C' has won)
                return result  # don't need to look anymore, send winning item/player ('P' or 'C')
    return "0"  # if no one had won, return '0'


def win_cond(test_line):
    # test_line is a dictionary with column indices as keys
    count = 1  # variable for counting consecutive items (1 for current item)
    for index in range(BOARD_WIDTH):  # loop through column indices
        if index > 0:  # if column is not first
            if (index - 1) in test_line and index in test_line:  # if line has items in both previous and curr columns
                if test_line[index - 1] == test_line[index]:  # if item in previous column equals one in current column
                    count += 1  # increment count
                    if count == 4:  # if count has reached 4, winning line has been found, game is over
                        return test_line[index]  # return winning item/player
                else:  # restart counting because different item is encountered
                    count = 1
            else:  # restart counting because test_line has no item in column
                count = 1
    return "0"  # if no one has won, return "0"


def evaluate(board, last_col, curr_depth, max_depth):
    pot_win = check_winner(board, last_col)
    if pot_win == "C":
        return 1
    elif pot_win == "P":
        return -1
    if curr_depth == max_depth:
        return 0
    curr_move = "C" if curr_depth % 2 == 1 else "P"
    total = 0
    all_lost, all_won = True, True
    for col in range(BOARD_WIDTH):
        board[col][len(board[col])] = curr_move
        result = evaluate(board, col, curr_depth + 1, max_depth)
        board[col].pop(len(board[col]) - 1, None)
        if result > -1:
            all_lost = False
        if result != 1:
            all_won = False
        if result == 1 and curr_move == "C":
            return 1
        if result == -1 and curr_move == "P":
            return -1
        total += result
    if all_won:
        return 1
    if all_lost:
        return -1
    total /= 7
    return total


class Game:
    def __init__(self, width):
        self.board = []  # game board is a list of columns
        [self.board.append({}) for w in range(width)]  # number of columns is the board width (default = 7)

    def enter(self, col, item):  # entering char ('P' or 'C') in the desired column
        self.board[col][len(self.board[col])] = item
        # dictionary length of selected column is used to determine position of item (always on top of last item in col)

    def __repr__(self):  # Board output format
        result = ""
        max_row = 7  # start with 7 rows
        for col in self.board:
            if len(col) > max_row:  # if a column has more rows, new max is that number
                max_row = len(col)
        for row in range(max_row - 1, -1, -1):  # for every row from max to 0
            for col in self.board:  # for every column
                if row in col:  # if col (dict) has item in row
                    result += col[row]  # add selected item ('P' or 'C')
                else:  # if not, location is empty
                    result += "="  # add empty sign ('=')
            result += "\n"
        return result


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size
if size <= 1:
    print("size must be bigger than 1")
    exit(1)
if rank == 0:
    game = Game(BOARD_WIDTH)
    print(game)
    while 1:  # loop game until someone wins
        while 1:  # loop until input inside allowed bounds (column indices)
            col_num = int(input())  # enter number from [0 - 6]
            if 0 <= col_num <= BOARD_WIDTH - 1:
                game.enter(int(col_num), "P")  # enter disc in selected column (height is not restricted)
                winner = check_winner(game.board, int(col_num))  # check if this move won the game
                if winner == "P":  # If it did, end the game
                    print(game)
                    # print("Player", winner, "has won!")
                    exit(1)
                break
        print(game)
        sys.stdout.flush()
        dest_rank = 1
        sent = 0
        start = time.time()
        for i in range(BOARD_WIDTH):
            comm.send(game.board, dest=dest_rank, tag=i)
            dest_rank += 1
            sent += 1
            if dest_rank == size:
                dest_rank = 1

        values = [0] * BOARD_WIDTH  # list for determining column with highest win quality
        value_string = ""
        for i in range(sent):
            st = MPI.Status()
            column_value = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=st)
            values[st.Get_tag()] = column_value
        print("time:", time.time() - start)
        for i in range(BOARD_WIDTH):
            value_string += str('%.3f' % values[i]) + " "
        print(value_string)
        move = values.index(max(values))
        game.enter(move, "C")
        winner = check_winner(game.board, move)
        if not winner == "0":
            print(game)
            # print("Player", winner, "has won!")
            exit(1)
        print(game)
else:
    st = MPI.Status()
    while 1:
        new_board = comm.recv(source=0, tag=MPI.ANY_TAG, status=st)
        col_index = st.Get_tag()
        new_board[col_index][len(new_board[col_index])] = "C"
        value = evaluate(new_board, col_index, 0, 6)
        new_board[col_index].pop(len(new_board[col_index]) - 1, None)
        comm.send(value, dest=0, tag=col_index)
